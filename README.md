# interFoamLS
This application is an etension of the interFoam application it is possible to find in the OpenFOAM routine.

## Addiction of the levelset

Adaptation of the Levelset algorithm it is possible to find [here](http://www.tfd.chalmers.se/~hani/kurser/OS_CFD_2015/SankarMenon/Report_SankarMenon.pdf)
 
 The *Level-set* function is initialized following:

 ```math

\phi_0=\left(1-2\alpha\right)\Gamma

 ```

 where $`\Gamma=0.65 \Delta x`$ and $`\delta x`$ is the dimension of each cell computed starting from the volume. Starting from the initial guess the redestancing function:

 ```math

\frac{\partial \phi}{\partial t} = S\left(\phi_0\right)\left(1-\nabla \phi\right)

 ```
 It would give a smoother level-set function.

## Curvature computation

Adaptation of the algorithm it is possible to find [here](https://www.researchgate.net/publication/4046804_Curvature-Based_Transfer_Functions_for_Direct_Volume_Rendering_Methods_and_Applications)
 
- Calculation of the normal to the level-set function

- Calculation of the Hessian (H) and Gauss matrix (G)

- Compute the trace T and Frobenius norm F of G. Then:

 ```math

k_1=\frac{T+\sqrt{2F^2-T^2}}{2}

 ```

 ```math

k_2=\frac{T-\sqrt{2F^2-T^2}}{2}

 ```



